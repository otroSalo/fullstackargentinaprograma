from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.middleware.csrf import get_token
from django.http import JsonResponse
from django.contrib.auth.views import LoginView
from django.contrib.auth import logout

# Create your views here.
def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()

            #return redirect('/accounts/login')
            return JsonResponse({'message':'Usuario registrado exitosamente'})
        else:
            errors = form.errors.as_json()
            return JsonResponse({'errors':errors},status=400)
        
    else:
        form = UserCreationForm()

    return render(request, 'registration/register.html',{'form': form})

def get_csrf_token(request):
    csrf_token = get_token(request)
    return JsonResponse({'csrf_token':csrf_token})


class CustomLoginView(LoginView):
    def form_valid(self, form):
        super().form_valid(form)
        return JsonResponse({'message': 'Inicio de sesion exitoso'})

    def form_invalid(self, form):
        super().form_invalid(form)
        return JsonResponse({'message': 'Inicio de sesion fallido'})
    
def custom_logout(request):
    logout(request)
    return JsonResponse({'message':'Logout exitoso'})