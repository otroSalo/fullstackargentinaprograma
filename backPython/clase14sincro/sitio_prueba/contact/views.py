from django.shortcuts import render, redirect, HttpResponse
from django.urls import reverse
from .forms import ContactForm
# Create your views here.
def contact(request):
    metodo = request.method
    
    if metodo == 'POST':
        contact_form = ContactForm(request.POST)
        if contact_form.is_valid():
            nombre =contact_form.cleaned_data['nombre']
        #nombre = request.POST.get('nombre','')
            
        return redirect(reverse('contact')+'?ok')
    else:
        contact_form= ContactForm()

    return render(request,"contact/contact.html",{'form': contact_form})
    #return HttpResponse("Espero envio de nombre")

def contador_visitas(request):
    contador = request.session.get('contador',0)

    contador+=1

    request.session['contador']=contador

    return render(request,'contador/contador.html',{'contador':contador})