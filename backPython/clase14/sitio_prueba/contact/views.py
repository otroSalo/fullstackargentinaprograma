# contact/views.py
from django.shortcuts import render, redirect, HttpResponse
from django.urls import reverse
from .forms import ContactForm

def contact(request):
    if request.method == "POST":
        #contact_form = ContactForm(request.POST)
        #if contact_form.is_valid():
            name =  request.POST.get('nombre', '') #contact_form.cleaned_data['name']
            email = request.POST.get('correo', '') #contact_form.cleaned_data['email']
            content = request.POST.get('mensaje', '') #contact_form.cleaned_data['content']
            # Procesar los datos, por ejemplo, enviar un correo electrónico
            # Luego, redirigir al usuario a una página de confirmación
            return redirect(reverse('contact') + "?ok")

    #else:
        #contact_form = ContactForm()

    return render(request, "contact/contact.html")#, {'form': contact_form})


def contador_visitas(request):
    # Inicializar la variable contador en 0 si no existe en la sesión
    contador = request.session.get('contador', 0)
    
    # Incrementar el contador
    contador += 1
    
    # Guardar el contador actualizado en la sesión
    request.session['contador'] = contador
    
    return render(request, 'contador/contador.html', {'contador': contador})

def clear_session(request):
    # Limpia la información de la sesión
    request.session.clear()

    return HttpResponse("Información de sesión eliminada")