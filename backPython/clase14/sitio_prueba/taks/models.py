from django.contrib.auth.models import User
from django.db import models


class Task(models.Model):
    titulo = models.CharField(max_length=255)
    detalle = models.TextField()
    id_usuario = models.ForeignKey(User, on_delete=models.CASCADE, related_name='tasks')  # Agrega related_name

