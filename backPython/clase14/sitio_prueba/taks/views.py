from django.shortcuts import render
from .models import Task
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def taks(request):
    #if request.user.is_authenticated:
        tareas = Task.objects.filter(id_usuario=request.user)
        return render(request, "taks/taks.html",{'tareas':tareas})
    #else:
    #    return render(request, "taks/taks.html")
