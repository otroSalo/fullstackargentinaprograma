from django.urls import path, include
from rest_framework.routers import DefaultRouter 
from libros import views
# Cree un enrutador y registre nuestros conjuntos de vistas con él.
#router = DefaultRouter() 
#router.register(r'libros', views.LibroViewSet) 

# Las URL de la API ahora las determina automáticamente el enrutador.
urlpatterns = [
    #path('', include(router.urls)),
    path('api/libros/', views.LibroListView.as_view(), name='lista-libro'),
    path('api/libros/<int:pk>/', views.LibroDetailView.as_view(), name='libro-detalle'),    
]

#GET libros/ -> nos trae todos los libros
#POST libros/ -> guardarlibro

#GET libros/1/ -> nos trae detalle de un solo libro
#PUT libros/1/ -> actualiza libro
#DELETE libros/1/ -> elimina libro