from django.shortcuts import render

# Create your views here.
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from .models import Libro
from .serializers import LibroSerializer

@api_view(['GET'])
def libro_list(request):
    """
    Vista personalizada para listar todos los libros.
    """
    if request.method == 'GET':
        libros = Libro.objects.all()
        serializer = LibroSerializer(libros, many=True)
        return Response(serializer.data)

@api_view(['GET'])
def libro_detail(request, pk):
    """
    Vista personalizada para obtener detalles de un libro por su ID o crear un nuevo libro.
    """
    if request.method == 'GET':
        try:
            libro = Libro.objects.get(pk=pk)
        except Libro.DoesNotExist:
            libro = None

        if libro is not None:
            serializer = LibroSerializer(libro)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['POST'])
def libro_create(request):
    """
    Vista personalizada para crear un nuevo libro.
    """
    if request.method == 'POST':
        serializer = LibroSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)