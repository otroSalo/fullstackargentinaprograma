## Instalando DRF

python -m venv .venv
.venv\Scripts\activate
python -m pip install --upgrade pip
pip install django
pip install djangorestframework

django-admin startproject nueva_api

python manage.py migrate
python manage.py startapp libros
-- configurar app en settings.py
python manage.py makemigrations
python manage.py migrate

python manage.py createsuperuser
python manage.py runserver

## Serializador

-- creamos nuestro modelo "Libros" en model.py

-- hacemos la migracion
python manage.py makemigrations
python manage.py migrate

-- creamos una clase para serializar en serializers.py , qeu extienda de serializers.Serializer

-- refactorizamos la clase y que extienda de serializers.ModelSerializar

-- agregamos el nuevo modelo "libro" en admin.py para usar la interfaz

-- probamos la serializacion desde shell de python
python manage.py shell
from libros.models import Libro
from libros.serializers import LibroSerializer
from rest_framework.renderers import JSONRenderer 
from rest_framework.parsers import JSONParser

libro = Libro.objects.get(id=1)
serializer = LibroSerializer(libro) 
serializer.data
content = JSONRenderer().render(serializer.data) content

import io
stream = io.BytesIO(content)
data = JSONParser().parse(stream)

## Viewsets y routers
ViewSet son casi lo mismo que las clases View, excepto que proporcionan operaciones como 
retrieve o update, y no manejadores de métodos como get o put

En primer lugar, refactoricemos nuestras vistas UserList y UserDetail en un único UserViewSet. 
Podemos eliminar las dos vistas y reemplazarlas con una sola clase: 