from django.shortcuts import render , HttpResponse
from .models import UserProfile

# Create your views here.
def holaMundo(request):
    #html_response = "<h1>Hola mundo</h1>"
    #for i in range(10):
    #    html_response += "<p>Linea "+str(i)+ "</p>"
    #return HttpResponse(html_response)

    #invocar nuetro modelo
    #devolver json
    return render(request,"prueba/holamundo.html")

def user_list(request):
    usuarios = UserProfile.objects.all()
    #usuarios = [{'nombre':'Juan'},{'nombre':'Ana'},{'nombre':'Lucia'}]
    return render(request, 'prueba/user_list.html', {'usuarios':usuarios})