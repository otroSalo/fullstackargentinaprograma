from django.db import models

# Create your models here.
from django.db import models

class UserProfile(models.Model):
    nombre = models.CharField(max_length=50)
    email = models.EmailField()

    def __str__(self):
        return self.username
