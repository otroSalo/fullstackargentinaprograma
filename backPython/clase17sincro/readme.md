## Instalando DRF

python -m venv .venv
.venv\Scripts\activate
python -m pip install --upgrade pip
pip install django
pip install djangorestframework

django-admin startproject nueva_api

python manage.py migrate
python manage.py createsuperuser
python manage.py runserver


python manage.py startapp libros
-- configurar app en settings.py

-- creamos nuestro modelo "Libros" en models.py

-- hacemos la migracion
python manage.py makemigrations
python manage.py migrate

-- agregamos el nuevo modelo "libro" en admin.py para usar la interfaz

## Serializador

-- creamos una clase para serializar en serializers.py , qeu extienda de serializers.Serializer

-- refactorizamos la clase y que extienda de serializers.ModelSerializar

-- Hacemos las vistas

-- Cargamos las urls

## Request y Response

-- Request Object
request.POST # Solo maneja datos de formulario. Solo funciona para el método 'POST'.
request.data # Maneja datos arbitrarios. Funciona para los métodos 'POST', 'PUT' y
'PATCH'.

-- Codigos de estado (Status Code)
DRF proporciona identificadores más explícitos para cada código de estado, como HTTP_400_BAD_REQUEST en el módulo de estado

-- Empaquetado API views  (Wrapping)
DRF proporciona dos contenedores que se puede usar para escribir vistas de API.
El decorador @api_view para trabajar con vistas basadas en funciones. La clase APIView para trabajar con vistas basadas en clases.

-- En el codigo, pasamos de:

--- vistas definidas por funciones 
@api_view(['GET'])
def libro_list(request):
    """
    Vista personalizada para listar todos los libros.
    """
    if request.method == 'GET':
        libros = Libro.objects.all()
        serializer = LibroSerializer(libros, many=True)
        return Response(serializer.data)

--- vistas definidas por clases
class LibroList(generics.ListCreateAPIView):
    queryset = Libro.objects.all()
    serializer_class = LibroSerializer

--- crud pre definido
class LibroViewSet(viewsets.ModelViewSet):
    queryset = Libro.objects.all()
    serializer_class = LibroSerializer

    def perform_create(self, serializer):
        serializer.save()

GET a /libros/: Lista de todos los libros.
POST a /libros/: Crear un nuevo libro.
GET a /libros/{ID}/: Obtener detalles de un libro por su ID.
PUT y DELETE a /libros/{ID}/: Actualizar y eliminar un libro por su ID.

## Autenticacion y Permisos

• Los libros siempre están asociados con un creador.
• Solo los usuarios autenticados pueden crear libros.
• Solo el creador de un libro puede actualizarlo o eliminarlo.
• Las solicitudes no autenticadas deben tener acceso completo de solo lectura. 

-- Modificando modelo Libro - Relacion a usuario

-- Hacemos las migraciones (definimos usuario default a los libros ya creados)
python manage.py makemigrations
python manage.py migrate

Python mange.py runserver
-- Aprovechamos a ir al admin y crear algunos usuarios 

-- Creamos Serializador del Usuario
Es necesario mostrar los libros que tiene ese usuario?!

-- Creamos un vista generica para los usuarios

-- Agregamos la direcciones para gestionar usuarios

-- Probamos!!! Observar que al crear libro no tenemos asociado la creacion con ningu usuario. asi que refactorizamos la vista post para guardar un libro.

-- actualizamos nuestro libroSerializer asi agregamos al usuario

-- una vez relacionado todo OK, necesitamos habilitar permisos de solo lectura a nuestras vistas

------------- HAsta aca podemos ver que cada pegada al endpoint no nos habilita crea una session!!! por que?!

Autenticación Basada en Sesiones
La autenticación basada en sesiones es un enfoque en el que el servidor crea una sesión para el usuario después de que este se autentique correctamente (por ejemplo, con nombre de usuario y contraseña). El servidor genera una cookie de sesión que se almacena en el navegador del cliente y se usa para identificar al usuario en cada solicitud posterior.

Autenticación Basada en Tokens JWT
La autenticación basada en tokens JWT utiliza tokens JWT (JSON Web Tokens) para autenticar a los usuarios. Cuando un usuario se autentica correctamente, el servidor genera un token JWT y lo envía al cliente. El cliente almacena el token y lo incluye en el encabezado de cada solicitud subsiguiente.

## Configuracion cors para consumir desde front

pip install django-cors-headers

INSTALLED_APPS = [
    # ...
    'corsheaders',
    # ...
]

MIDDLEWARE = [
    # ...
    'corsheaders.middleware.CorsMiddleware',
    # ...
]

CORS_ALLOWED_ORIGINS = [
    "http://localhost:3000",
]

