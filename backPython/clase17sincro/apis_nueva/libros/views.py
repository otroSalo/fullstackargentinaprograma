from django.shortcuts import render
from .models import Libro
from .serializers import LibroSerializer, UserSerializer
from rest_framework import status, viewsets , generics, permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth.models import User
# Create your views here.

#Funcion que enlista
# @api_view(['GET'])
# def libro_list(request):
#     """
#     Vista personalizada para listar todos los libros.
#     """
#     if request.method == 'GET':
#         libros = Libro.objects.all()
#         serializer = LibroSerializer(libros, many=True)
#         return Response(serializer.data)

#clase que hace una sola cosa
# class LibroList(generics.ListCreateAPIView):
#     queryset = Libro.objects.all() 
#     serializer_class = LibroSerializer

#una clase que hace todo crud
# class LibroViewSet(viewsets.ModelViewSet):
#     queryset = Libro.objects.all() 
#     serializer_class = LibroSerializer

class LibroListView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    #GET libros/ -> nos trae todos los libros
    def get(self,request):
        libros = Libro.objects.filter(owner=request.user)
        serializer = LibroSerializer(libros, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    
    #POST libros/ -> guardarlibro
    def post(self, request):
        serializer = LibroSerializer(data=request.data)
        if serializer.is_valid():
            #serializer.save()
            serializer.save(owner=self.request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class LibroDetailView(APIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    #GET libros/1/ -> nos tre detalle de un solo libro
    def get(self,request,pk):
        libro = Libro.objects.get(pk=pk)
        serializer = LibroSerializer(libro)
        if libro:
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)

    #PUT libros/1/ -> actualiza libro
    def put(self,request,pk):
        libro = Libro.objects.get(pk=pk)
        if not libro:
            return Response({'error':'Libro no encontrado'}, status=status.HTTP_404_NOT_FOUND)
            
        serializer = LibroSerializer(libro, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
       
        return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)
    
    #DELETE libros/1/ -> elimina libro
    def delete(self, request, pk):
        libro = Libro.objects.get(pk=pk)
        if not libro:
            return Response({'error':'Libro no encontrado'}, status=status.HTTP_404_NOT_FOUND)
        
        libro.delete()
        return Response({'mensaje':'Libro eliminado correctamente'},status=status.HTTP_204_NO_CONTENT)
    
#clase para manejar múltiples instancias 
class UserList(generics.ListAPIView): 
    queryset =User.objects.all()
    serializer_class = UserSerializer

#clase para manejar una única instancia 
class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from django.views.decorators.csrf import csrf_exempt

class LoginView(APIView):
    authentication_classes = [BasicAuthentication]  # Utiliza autenticación básica
    permission_classes = [IsAuthenticated]  # Requiere autenticación

    def post(self, request):
        # El usuario ha sido autenticado correctamente
        return Response({'message': 'Inicio de sesión exitoso'}, status=status.HTTP_200_OK)