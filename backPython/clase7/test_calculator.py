# test_calculator.py
import calculator

def test_add():
    result = calculator.add(3, 5)
    assert result == 8

def test_subtract():
    result = calculator.subtract(10, 4)
    assert result == 6
