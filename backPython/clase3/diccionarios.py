# Creación de un diccionario
mi_diccionario = {'nombre': 'Juan', 'edad': 30, 'ocupacion': 'ingeniero'}

# Acceso a elementos por clave
nombre_persona = mi_diccionario['nombre']  # Obtendrá el valor 'Juan'
edad_persona = mi_diccionario['edad']  # Obtendrá el valor 30

# Mostrar el diccionario completo
print(mi_diccionario)  # Salida: {'nombre': 'Juan', 'edad': 30, 'ocupacion': 'ingeniero'}

# Modificación de elementos
mi_diccionario['edad'] = 35  # Cambiamos el valor de la clave 'edad' a 35

# Mostrar el diccionario después de la modificación
print(mi_diccionario)  # Salida: {'nombre': 'Juan', 'edad': 35, 'ocupacion': 'ingeniero'}


# Los diccionarios son ideales para almacenar y recuperar información utilizando claves descriptivas.
# Por ejemplo, un diccionario para almacenar información de estudiantes:

estudiante1 = {'nombre': 'María', 'edad': 25, 'curso': 'Matemáticas', 'promedio': 8.7}
estudiante2 = {'nombre': 'Carlos', 'edad': 22, 'curso': 'Historia', 'promedio': 7.5}

# Acceso a elementos por clave
nombre_estudiante1 = estudiante1['nombre']
curso_estudiante2 = estudiante2['curso']

print("Nombre del estudiante 1:", nombre_estudiante1)  # Salida: Nombre del estudiante 1: María
print("Curso del estudiante 2:", curso_estudiante2)  # Salida: Curso del estudiante 2: Historia

# Modificación de elementos
estudiante1['promedio'] = 9.2  # Actualizamos el promedio del estudiante 1

# Mostrar el diccionario del estudiante 1 después de la modificación
print(estudiante1)  # Salida: {'nombre': 'María', 'edad': 25, 'curso': 'Matemáticas', 'promedio': 9.2}
