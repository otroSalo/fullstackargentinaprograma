# Implementación de una cola utilizando listas en Python
cola = []

# Operación de encolar (enqueue)
cola.append(1)
cola.append(2)
cola.append(3)

# Mostrar la cola actual
print("Cola actual:", cola)  # Salida: Cola actual: [1, 2, 3]

# Operación de desencolar (dequeue)
elemento_desencolado = cola.pop(0)
print("Elemento desencolado:", elemento_desencolado)  # Salida: Elemento desencolado: 1

# Mostrar la cola después de desencolar
print("Cola después de desencolar:", cola)  # Salida: Cola después de desencolar: [2, 3]


# Utilidad de las colas: Simulación de procesamiento de tareas en un sistema

def procesar_tareas(cola_tareas):
    tiempo_total = 0

    while len(cola_tareas) > 0:
        tarea_actual = cola_tareas.pop(0)
        tiempo_procesamiento_tarea = procesar_tarea(tarea_actual)
        tiempo_total += tiempo_procesamiento_tarea

    return tiempo_total

def procesar_tarea(tarea):
    # Simulación del tiempo de procesamiento de una tarea (en segundos)
    # Aquí podrías realizar operaciones más complejas relacionadas con la tarea
    return tarea * 2

# Cola de tareas a procesar (tiempo estimado de cada tarea en segundos)
cola_tareas = [5, 3, 7, 2, 1]

# Procesar las tareas en la cola
tiempo_total_procesamiento = procesar_tareas(cola_tareas)
print("Tiempo total de procesamiento:", tiempo_total_procesamiento, "segundos")

