# Implementación de una pila utilizando una lista en Python
pila = []

# Operación de apilar (push)
pila.append(1)
pila.append(2)
pila.append(3)

# Mostrar la pila actual
print("Pila actual:", pila)  # Salida: Pila actual: [1, 2, 3]

# Operación de desapilar (pop)
elemento_desapilado = pila.pop()
print("Elemento desapilado:", elemento_desapilado)  # Salida: Elemento desapilado: 3

# Mostrar la pila después de desapilar
print("Pila después de desapilar:", pila)  # Salida: Pila después de desapilar: [1, 2]

# Las pilas son útiles para muchas situaciones, incluyendo la evaluación de expresiones matemáticas.
# Por ejemplo, evaluación de una expresión matemática en notación polaca inversa (RPN):

def evaluar_expresion_rpn(expresion):
    pila = []

    for token in expresion.split():
        if token.isdigit():  # Si es un número, lo apilamos
            pila.append(int(token))
        else:  # Si es un operador, realizamos la operación con los dos últimos elementos apilados
            segundo_numero = pila.pop()
            primer_numero = pila.pop()
            if token == '+':
                pila.append(primer_numero + segundo_numero)
            elif token == '-':
                pila.append(primer_numero - segundo_numero)
            elif token == '*':
                pila.append(primer_numero * segundo_numero)
            elif token == '/':
                pila.append(primer_numero / segundo_numero)

    return pila[0]  # El resultado final es el único elemento que queda en la pila

# Evaluamos la expresión "3 4 + 5 6 + *"
resultado = evaluar_expresion_rpn("3 4 + 5 6 + *")
print("Resultado:", resultado)  # Salida: Resultado: 77 (resultado de (3 + 4) * (5 + 6))
