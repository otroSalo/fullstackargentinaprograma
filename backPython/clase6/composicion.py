# La composición es un enfoque donde un objeto contiene otros objetos como parte de su estructura.

class Motor:
    def __init__(self, tipo):
        self.tipo = tipo

    def encender(self):
        print(f"Motor {self.tipo} encendido")

    def apagar(self):
        print(f"Motor {self.tipo} apagado")

class Coche:
    def __init__(self, marca, modelo, tipo_motor):
        self.marca = marca
        self.modelo = modelo
        self.motor = Motor(tipo_motor)

    def arrancar(self):
        print(f"{self.marca} {self.modelo} arrancando")
        self.motor.encender()

    def apagar(self):
        self.motor.apagar()
        print(f"{self.marca} {self.modelo} apagado")

# Crear un coche con un motor eléctrico
coche_electrico = Coche("Tesla", "Model 3", "eléctrico")
coche_electrico.arrancar()
coche_electrico.apagar()
