class Vehiculo:
    def __init__(self, marca, modelo):
        self.__marca = marca
        self.__modelo = modelo

    def obtener_info(self):
        return f"Marca: {self.__marca}, Modelo: {self.__modelo}"

class Automovil(Vehiculo):
    def __init__(self, marca, modelo, color):
        super().__init__(marca, modelo)
        self.__color = color

    def obtener_info(self):
        info_padre = super().obtener_info()
        return f"{info_padre}, Color: {self.__color}"

class Moto(Vehiculo):
    def __init__(self, marca, modelo, cilindrada):
        super().__init__(marca, modelo)
        self.__cilindrada = cilindrada

    def obtener_info(self):
        info_padre = super().obtener_info()
        return f"{info_padre}, Cilindrada: {self.__cilindrada} cc"

class Concesionario:
    def __init__(self):
        self.__vehiculos = []

    def agregar_vehiculo(self, vehiculo):
        self.__vehiculos.append(vehiculo)

    def mostrar_inventario(self):
        print("Inventario del Concesionario:")
        for vehiculo in self.__vehiculos:
            print(vehiculo.obtener_info())

auto1 = Automovil("Toyota", "Corolla", "Rojo")
moto1 = Moto("Honda", "CBR500R", 500)

concesionario = Concesionario()
concesionario.agregar_vehiculo(auto1)
concesionario.agregar_vehiculo(moto1)

concesionario.mostrar_inventario()
