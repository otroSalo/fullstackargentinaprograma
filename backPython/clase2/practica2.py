# Funciones
def saludar():
    print("Hola, ¡bienvenido!")

saludar() # Imprime "Hola, ¡bienvenido!"

## COn argumentos
def suma(a, b):
    return a + b

resultado = suma(5, 3)
print(resultado) # Imprime 8


#Algunas funciones comunes son: 
# print(), input(), len(), range(), type(), 
# str(), int(), float(), list(), tuple(), set(), 
# dict(), max(), min(), sum(), upper(), lower(), 
# capitalize(), strip(), split(), join(), replace().

texto = "Hola, mundo!"
print(len(texto)) # Imprime 12

numeros = [1, 2, 3, 4, 5]
suma = sum(numeros)
print(suma) # Imprime 15


#Estructura de control
edad = 18
if edad >= 18:
    print("Eres mayor de edad")
else:
    print("Eres menor de edad")

#Ejemplo de for
numeros = [1, 2, 3, 4, 5]
for numero in numeros:
    print(numero)

contador = 0
while contador < 5:
    print(contador)
    contador += 1
