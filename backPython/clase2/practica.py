#HolaMundo!

print("¡Hola, Mundo!")


#Calculo de area de un circulo

import math

radio = 5
area = math.pi * radio*2
print("El area de circulo es:",area)
print(f"El area de circulo es: {area}")


#Suma de numeros en una lista
numeros = [1, 2, 3, 4, 5]
suma = sum(numeros)
print(f"La suma de los números en la lista es: {suma}")


#Comprobar si un numero es par o impar
numero = 7
if numero % 2 == 0:
    print("El número es par.")
else:
    print("El número es impar.")


#Bucle for para recorrer una lista
frutas = ["manzana", "plátano", "naranja"]
for fruta in frutas:
    print(fruta)

for i in range(0,len(frutas)):
    print(frutas[i])


#Uso de funciones
def saludar(nombre):
    print(f"Hola, {nombre}!")

saludar("Juan")

#Leer y escribir en un archivo
# Escribir en un archivo
with open("archivo.txt", "w") as archivo:
    archivo.write("Hola, este es un ejemplo de escritura en un archivo.")

# Leer desde el archivo
with open("archivo.txt", "r") as archivo:
    contenido = archivo.read()
    print(contenido)





