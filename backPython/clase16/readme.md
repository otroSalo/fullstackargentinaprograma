## Instalando DRF

python -m venv .venv
.venv\Scripts\activate
python -m pip install --upgrade pip
pip install django
pip install djangorestframework

django-admin startproject nueva_api

python manage.py migrate
python manage.py createsuperuser
python manage.py runserver


python manage.py startapp libros
-- configurar app en settings.py

-- creamos nuestro modelo "Libros" en models.py

-- hacemos la migracion
python manage.py makemigrations
python manage.py migrate

-- agregamos el nuevo modelo "libro" en admin.py para usar la interfaz

## Serializador

-- creamos una clase para serializar en serializers.py , qeu extienda de serializers.Serializer

-- refactorizamos la clase y que extienda de serializers.ModelSerializar

-- Hacemos las vistas

-- Cargamos las urls

## Request y Response

-- Request Object
request.POST # Solo maneja datos de formulario. Solo funciona para el método 'POST'.
request.data # Maneja datos arbitrarios. Funciona para los métodos 'POST', 'PUT' y
'PATCH'.

-- Codigos de estado (Status Code)
DRF proporciona identificadores más explícitos para cada código de estado, como HTTP_400_BAD_REQUEST en el módulo de estado

-- Empaquetado API views  (Wrapping)
DRF proporciona dos contenedores que se puede usar para escribir vistas de API.
El decorador @api_view para trabajar con vistas basadas en funciones. La clase APIView para trabajar con vistas basadas en clases.

-- En el codigo, pasamos de:

--- vistas definidas por funciones 
@api_view(['GET'])
def libro_list(request):
    """
    Vista personalizada para listar todos los libros.
    """
    if request.method == 'GET':
        libros = Libro.objects.all()
        serializer = LibroSerializer(libros, many=True)
        return Response(serializer.data)

--- vistas definidas por clases
class LibroList(generics.ListCreateAPIView):
    queryset = Libro.objects.all()
    serializer_class = LibroSerializer

--- crud pre definido
class LibroViewSet(viewsets.ModelViewSet):
    queryset = Libro.objects.all()
    serializer_class = LibroSerializer

    def perform_create(self, serializer):
        serializer.save()

GET a /libros/: Lista de todos los libros.
POST a /libros/: Crear un nuevo libro.
GET a /libros/{ID}/: Obtener detalles de un libro por su ID.
PUT y DELETE a /libros/{ID}/: Actualizar y eliminar un libro por su ID.

