from django.shortcuts import render

# Create your views here.
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status, generics, viewsets, permissions
from .models import Libro
from .serializers import LibroSerializer


# @api_view(['GET'])
# def libro_list(request):
#     """
#     Vista personalizada para listar todos los libros.
#     """
#     if request.method == 'GET':
#         libros = Libro.objects.all()
#         serializer = LibroSerializer(libros, many=True)
#         return Response(serializer.data)


# class LibroList(generics.ListCreateAPIView):
#     queryset = Libro.objects.all()
#     serializer_class = LibroSerializer


# @api_view(['POST'])
# def libro_create(request):
#     """
#     Vista personalizada para crear un nuevo libro.
#     """
#     if request.method == 'POST':
#         serializer = LibroSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class LibroViewSet(viewsets.ModelViewSet):
    queryset = Libro.objects.all()
    serializer_class = LibroSerializer
    #permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def perform_create(self, serializer):
        #serializer.save(owner=self.request.user)
        serializer.save()