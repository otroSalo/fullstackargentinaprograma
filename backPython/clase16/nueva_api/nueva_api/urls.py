"""
URL configuration for nueva_api project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
#from libros.views import LibroList, libro_create, LibroViewSet #
from libros.views import LibroViewSet 

from rest_framework.routers import DefaultRouter #

router = DefaultRouter()#
router.register(r'libros', LibroViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    #path('libros/', LibroList.as_view(), name='libro-list'),
    #path('libros/crear/', libro_create, name='libro-create'),
    path('', include(router.urls)),
]
