-- subconsultas --
SELECT NOMBRE FROM CLIENTES
WHERE ID IN 
(SELECT CLIENTE_ID FROM CONTACTOS
WHERE EMAIL LIKE '%@example.com')

-- case --

SELECT CLIENTE_ID, 
CASE WHEN EMAIL LIKE '%@example.com'
THEN 'CORREO VALIDO'
ELSE 'CORREO INVALIDO'
END AS ESTADO_CORREO 
FROM CONTACTOS

-- insert --

insert into clientes (nombre, pais, ciudad, genero, edad) 
values ('Pedro', 'Canada', 'ciudad canada', 'masculino', '25')

INSERT INTO productos (NOMBRE)
VALUES ('PLAN FAMILIAR'),
('PLAN VACACIONES'),
('PLAN FINDE LARGO'),
('PLAN NORMAL')

-- actualizacion --

update clientes set id = 6 where nombre = 'Pedro'

UPDATE productos SET PRECIO = 120 WHERE NOMBRE = 'PLAN FAMILIAR'

