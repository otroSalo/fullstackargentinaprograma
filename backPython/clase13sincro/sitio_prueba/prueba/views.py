from django.shortcuts import render , HttpResponse 
from prueba.models import Usuarios
from django.http import JsonResponse
# Create your views here.
def holaMundo(request):
    #html_response = "<h1>Hola mundo</h1>"
    #for i in range(10):
    #    html_response += "<p>Linea "+str(i)+ "</p>"
    #return HttpResponse(html_response)

    #invocar nuetro modelo
    #devolver json
    return render(request,"prueba/holamundo.html")

def usuarios(request):
    usuarios = Usuarios.objects.all()
    #usuarios = [{'nombre':'Juan'},{'nombre':'Anna'},{'nombre':'Jorge'}]
    return render(request,"prueba/usuarios.html",{'usuarios':usuarios})

def agregar_usuario(request):
    if request.method == 'POST':
        nombre = request.POST.get('nombre', '')  # Obtiene el nombre del POST
        email = request.POST.get('email', '')  # Obtiene la contraseña del POST

        # Verifica si el nombre ya existe en la base de datos
        if Usuarios.objects.filter(nombre=nombre).exists():
            return JsonResponse({'message': 'Este nombre de usuario ya está en uso.'}, status=400)

        # Crea un nuevo usuario utilizando el método create
        nuevo_usuario = Usuarios.objects.create(nombre=nombre, email=email)

        return JsonResponse({'message': 'Usuario registrado con éxito!!!.'})
    else:
        return JsonResponse({'message': 'Método no permitido.'}, status=405)