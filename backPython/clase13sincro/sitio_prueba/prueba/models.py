from django.db import models

# Create your models here.

class Usuarios(models.Model):
    nombre = models.CharField(max_length=50)
    email = models.EmailField()

    def __str__(self):
        return self.nombre +' '+ self.email
    
class Tareas(models.Model):
    actividad = models.CharField(max_length=50)