# Intentamos dividir entre cero, lo que generará una excepción
try:
    resultado = 10 / 0
except ZeroDivisionError as error:
    print("Error: División entre cero.")


# Manejo de excepciones específicas
valor = "abc"
try:
    numero = int(valor)
except ValueError as error:
    print(f"Error: No se puede convertir '{valor}' a un número entero.")


# Manejo de múltiples excepciones
valor = "10"
try:
    resultado = 10 / int(valor)
except ValueError as error:
    print("Error de valor:", error)
except ZeroDivisionError as error:
    print("Error de división entre cero:", error)


# Uso de la cláusula else
try:
    resultado = 10 / 2
except ZeroDivisionError as error:
    print("Error de división entre cero:", error)
else:
    print("La división se realizó exitosamente. El resultado es:", resultado)


# Uso de la cláusula finally
archivo=False
try:
    archivo = open("archivo.txt", "r")
    contenido = archivo.read()
except FileNotFoundError as error:
    print("Error: El archivo no se encontró.")
else:
    print("Contenido del archivo:", contenido)
finally:
    if archivo:
        archivo.close()
    print("Liberando recursos.")
