# Programa principal que utiliza el módulo "operaciones"
import operaciones

resultado_suma = operaciones.suma(5, 3)
resultado_resta = operaciones.resta(10, 4)

print("Resultado de la suma:", resultado_suma)
print("Resultado de la resta:", resultado_resta)
