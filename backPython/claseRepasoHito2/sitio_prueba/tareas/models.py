from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Tareas(models.Model):
    titulo = models.CharField(max_length=255)
    detalle = models.TextField()
    id_usuario = models.ForeignKey(User,on_delete=models.CASCADE)