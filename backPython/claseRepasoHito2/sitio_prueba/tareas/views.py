from django.shortcuts import render
from .models import Tareas
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse

# Create your views here.
@login_required
def tareas(request):
    if request.method =='GET':
    #if request.user.is_authenticated:
        tareas= Tareas.objects.filter(id_usuario=request.user)
        tareas_data = [{'id': tarea.id, 'titulo': tarea.titulo, 'detalle': tarea.detalle} for tarea in tareas]
        return JsonResponse({'tareas': tareas_data})
        #return render(request,'tareas/tareas.html',{'tareas':tareas})
    #else:
    #    return render(request,'tareas/tareas.html')