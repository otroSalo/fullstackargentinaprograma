"""
URL configuration for sitio_prueba project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from tareas import views as viewsTareas
from registration import views as viewsRegistration

urlpatterns = [
    path('admin/', admin.site.urls),
    path('tareas/',viewsTareas.tareas, name='tareas'),
    #path('accounts/',include('django.contrib.auth.urls')),
    path('accounts/login/',viewsRegistration.CustomLoginView.as_view(),name='login' ),
    path('register/',viewsRegistration.register, name='register'),
    path('get-csrf-token/', viewsRegistration.get_csrf_token, name='get_csrf_token'),
    path('accounts/logout/', viewsRegistration.custom_logout, name='custom_logout'),
]
