from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm

# Create your views here.

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/accounts/login')
        
    else:
        form = UserCreationForm()
        
    return render(request, 'registration/register.html',{'form':form})


from django.contrib.auth.views import LoginView
from django.http import JsonResponse

class CustomLoginView(LoginView):
    def form_valid(self, form):
        # Lógica de inicio de sesión exitoso
        super().form_valid(form)
        return JsonResponse({'message': 'Inicio de sesión exitoso'})

    def form_invalid(self, form):
        # Lógica de inicio de sesión inválido
        super().form_invalid(form)
        return JsonResponse({'message': 'Inicio de sesión fallido'}, status=400)
    

from django.middleware.csrf import get_token
from django.http import JsonResponse

def get_csrf_token(request):
    csrf_token = get_token(request)
    return JsonResponse({'csrf_token': csrf_token})

from django.contrib.auth import logout
from django.http import JsonResponse

def custom_logout(request):
    logout(request)
    return JsonResponse({'message': 'Logout exitoso'})