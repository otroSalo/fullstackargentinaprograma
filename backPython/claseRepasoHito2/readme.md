## CrearProyecto

        python -m venv .venv
        .venv\Scripts\activate
        python -m pip install --upgrade pip
        pip install django
        django-admin startproject sitio_prueba
        cd mi_proyecto
        python manage.py runserver


## CrearAplicación Tareas

        python manage.py startapp tareas

        no te olvides de agregar la aplicacion a settings

## Creamos modelo, views, template "tareas"

        creamos class Tareas en models

        python manage.py makemigrations
        python manage.py migrate
    
        definir funcion en vista
        crear template
        agregamos url

        actualizamos vista para traer tareas de la bd
        actualizamos template para mostrarlo

## Vemos en base de datos

... usuarios y grupos /admin

- creamos super usuario para loguearnos en /admin de django
        python manage.py createsuperuser

  nos logueamos en /admin

  creamos 2 usuarios Agus y Hernan

  chequeamos la bd tabla auth_user y django_session

## CrearAplicaction Registros

        python manage.py startapp registration

        no te olvides de agregar la aplicacion a settings

## adecuamos configuracion, creamos template login y logout

## creamos model, views y tempalte registrarnos