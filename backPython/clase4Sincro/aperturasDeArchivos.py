nombre_archivo = "archivo_lectura.txt"
archivo = False
try:
    archivo = open(nombre_archivo, "r")
    contenido = archivo.read()
    print("Contenido del archivo:")
    print(contenido)
except FileNotFoundError:
    print(f"El archivo '{nombre_archivo}' no fue encontrado.")
finally:
    if archivo!=False :
        archivo.close()  # Siempre cierra el archivo después de usarlo


nombre_archivo = "archivo_escritura2.txt"

try:
    archivo = open(nombre_archivo, "r")
    archivo.write("Este es un ejemplo de escritura en archivo.\n")
    archivo.write("Podemos sobrescribir todo el contenido anterior.")
except:
    print("Ocurrió un error al escribir en el archivo.")
finally:
    archivo.close()


nombre_archivo = "archivo_agregar.txt"

try:
    archivo = open(nombre_archivo, "a")
    archivo.write("Este contenido se agregó al final del archivo.\n")
    archivo.write("Podemos seguir añadiendo más líneas.")
except:
    print("Ocurrió un error al agregar contenido al archivo.")
finally:
    archivo.close()

#-------------Ejemplo sobre el mismo archivo
nombre_archivo = "archivo_importante.txt"

try:
    archivo = open(nombre_archivo, "r")
    contenido = archivo.read()
    print("Contenido del archivo:")
    print(contenido)
except FileNotFoundError:
    print(f"El archivo '{nombre_archivo}' no fue encontrado.")
finally:
    archivo.close()  # Siempre cierra el archivo después de usarlo

# El archivo se cerró correctamente, por lo que podemos trabajar con él nuevamente
try:
    archivo = open(nombre_archivo, "a")
    archivo.write("Esta es una actualización importante.")
except:
    print("Ocurrió un error al escribir en el archivo.")
finally:
    archivo.close()
