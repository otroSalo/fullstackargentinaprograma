# Crear un nuevo archivo de texto plano y escribir contenido en él
with open("archivo_texto.txt", "w") as archivo:
    archivo.write("Este es un archivo de texto plano.\n")
    archivo.write("Puedes escribir varias líneas en él.\n")

# Leer y mostrar el contenido del archivo
with open("archivo_texto.txt", "r") as archivo:
    contenido = archivo.read()
    print(contenido)


import csv

# Crear un archivo CSV y escribir datos en él
datos = [["Nombre", "Edad"], ["Ana", 25], ["Juan", 30], ["María", 28]]
with open("archivo_csv.csv", "w", newline="") as archivo:
    escritor_csv = csv.writer(archivo)
    escritor_csv.writerows(datos)

# Leer y mostrar el contenido del archivo CSV
with open("archivo_csv.csv", "r") as archivo:
    lector_csv = csv.reader(archivo)
    for fila in lector_csv:
        print(fila)


# Leer una imagen binaria y copiarla en otro archivo
with open("imagen_original.jpg", "rb") as archivo_origen:
    datos_imagen = archivo_origen.read()
    with open("copia_imagen.jpg", "wb") as archivo_destino:
        archivo_destino.write(datos_imagen)


class Estudiante:
    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad

# Crear un archivo binario y escribir objetos Estudiante en él
estudiantes = [Estudiante("Ana", 20), Estudiante("Juan", 22), Estudiante("María", 21)]
with open("estudiantes.dat", "wb") as archivo:
    for estudiante in estudiantes:
        archivo.write(estudiante.nombre.encode("utf-8"))
        archivo.write(estudiante.edad.to_bytes(2, byteorder="big"))



# Leer y mostrar los objetos Estudiante desde el archivo binario
estudiantes_leidos = []
with open("estudiantes.dat", "rb") as archivo:
    while True:
        nombre = archivo.read(20).decode("utf-8").rstrip('\x00')
        if not nombre:
            break
        edad = int.from_bytes(archivo.read(2), byteorder="big")
        estudiantes_leidos.append(Estudiante(nombre, edad))

for estudiante in estudiantes_leidos:
    print(f"Nombre: {estudiante.nombre}, Edad: {estudiante.edad}")
