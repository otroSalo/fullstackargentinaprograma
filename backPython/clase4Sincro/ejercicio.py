#Hacer una funcion que copie de un archivo viejo a un archivo nuevo todo el texto, 
#OMITIENDO las lineas que comienzan con "#"

def filtraArchivo(archViejo, archNuevo):
    f1 = open(archViejo, "r")
    f2 = open(archNuevo, "w")
    while 1:
        texto = f1.readline()
        if texto == "":
            break
        if texto[0] == '#':
            continue
        f2.write(texto)
    f1.close()
    f2.close()
    return

filtraArchivo("archivoViejo.txt","archivoNuevo.txt")