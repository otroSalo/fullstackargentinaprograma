import unittest

class Calculadora:

    def resta(self,a,b):
        return a - b
    
    def suma(self,a,b):
        return a+b

    def dividir(self,a,b):
        if b==0:
            raise ValueError("No se puede dividir por cero")
        return a/b
    
class TestCalculadora(unittest.TestCase):
    def setUp(self):
        self.calculadora=Calculadora()
    
    def tearDown(self):
        self.calculadora = None

    def test_suma(self):
        result = self.calculadora.suma(3,5)
        self.assertEqual(result,8)

    def test_resta(self):
        result = self.calculadora.resta(3,5)
        self.assertEqual(result,-2)

if __name__ == '__main__':
    unittest.main()