
-- Inner Join ---
SELECT clientes.id ,clientes.nombre,clientes.genero,  contactos.telefono, contactos.email  from clientes
inner join contactos on clientes.id = contactos.cliente_id 

-- Full Outer Join --

SELECT clientes.id ,clientes.nombre,clientes.genero,  contactos.telefono, contactos.email  from clientes
full outer join contactos on clientes.id = contactos.cliente_id 

-- "Left Join" --

SELECT clientes.id ,clientes.nombre,clientes.genero,  contactos.telefono, contactos.email  from clientes
left join contactos on clientes.id = contactos.cliente_id 


-- group by--

Select genero, avg(edad) as promedio_edad from clientes
group by genero


-- HAVING--

SELECT GENERO, AVG(EDAD) AS PROMEDIO_EDAD FROM clientes
GROUP BY GENERO
HAVING PROMEDIO_EDAD >30

SELECT pais, COUNT(*) AS CANT_PROVEDORES FROM proveedores 
GROUP BY pais
HAVING CANT_PROVEDORES >1

