# BackEnd con Python

## Introducción a Python

Clase 1

## Funciones y Estructuras de Control

Clase 6

## Tipos de datos complejos

Clase 3

## Manejo de archivos

Clase 4

## Manejo de errores y excepciones
## Modulos y paquetes

Clase 5

## Programación orientada a objetos

Clase 6

## Objetos avanzados

Clase 6

## Pruebas Unitarias

Clase 7

## Intro. a Base de datos
## SQLITE
## Conectare a una base
## Consultar, ordenar y filtrar datos

Clase 8

## Joins Conjuntos

Clase 9

## Consultas complejas y Transacciones

Clase 10

## Vistas, índice y Triggers

Clase 11

## Intro. a Django

Clase 12

## ORM, MVC y Modelos

Clase 13

## Formularios, Sessiones y Autenticacion de usuarios

Clase 14

## Querysets y seguridad

Clase 15

## Introduccion DRF

Clase 16

## Api Post, Get, Delete y Put

Clase 17

## Autorización y permisos

Clase 18

## Relaciones de API

Clase 19
















